﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace ImgOptimizer
{
    class Menu
    {
        public static void ReadOption()
        {
            Console.WriteLine("########################################################################");
            Console.WriteLine("#################### Welcome to the image optimizer ####################");
            Console.WriteLine("########################################################################");
            Console.WriteLine("In order to optimize a directory you need to input the image's directory");
            Console.Write("> ");
            var cli = new OptimizerCLI(Console.ReadLine());
            Console.WriteLine(cli.WalkDirectory());
            Console.ReadLine();
        }
    }
}
