﻿using System;
using System.IO;
using System.Diagnostics;
using System.Text;


namespace ImgOptimizer
{
    class OptimizerCLI
    {
        public OptimizerCLI(string directoryName)
        {
            RootDirectory = new DirectoryInfo(directoryName);
        }
        public DirectoryInfo RootDirectory { get; set; }
        public string WalkDirectory(DirectoryInfo directory = null)
        {
            if (directory == null)
                directory = RootDirectory; 
            var subDirs = directory.GetDirectories();
            var files = HandleFiles(directory);
            var optimizedDir = optimizedDirectory(directory == RootDirectory ? null : directory);
            foreach (var file in files)
            {
                if (IsImage(file.FullName))
                {
                    // do the optimization
                    string cmd;
                    cmd = $@"/C convert {file.FullName} -strip -quality 45 {optimizedDir.FullName}\{file.Name}";
                    Process.Start("CMD.exe", cmd);
                    Console.WriteLine(file.Name + " optimized");
                }
            }
            foreach (var dir in subDirs)
            {
                // recursive navigate function to walk trough all the subFolders
                WalkDirectory(dir);
            }
            return $"Done Walking {RootDirectory.FullName}";
        }
        public FileInfo[] HandleFiles(DirectoryInfo directory)
        {
            FileInfo[] files = null;
            try
            {
                files = directory.GetFiles();
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("No Authorization on the RootDirectory, error:" + e.ToString());
                return null;
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine("Directory not found, error: " + e.ToString());
                return null;
            }
            return files;
        }
        public bool IsImage(string fileName)
        {
            var extension = fileName.Split('.')[1];
            if (extension.ToLower() == "jpeg" || extension.ToLower() == "png" || extension.ToLower() == "jpg")
            {
                return true;
            }
            return false;
        } 
        public DirectoryInfo optimizedDirectory(DirectoryInfo dir = null)
        {
            DirectoryInfo childDir = null;
            if (dir != null)
                childDir = dir;
            var parent = RootDirectory.Parent;
            var subDirs = parent.GetDirectories();
            DirectoryInfo optimizedDir;
            string cmd;
            if (!Directory.Exists(parent.FullName + $@"\{RootDirectory.Name}_optimized\{RootDirectory.Name}"))
            {
                cmd = $@"/C mkdir {parent.FullName}\{RootDirectory.Name}_optimized\{RootDirectory.Name}";
                try
                {
                    Process.Start("CMD.exe", cmd);
                }
                catch
                {
                    Console.WriteLine("could not create directory for optimized images"); 
                }
            }
            if (childDir != null)
                if (!Directory.Exists(parent.FullName + $@"\{RootDirectory.Name}_optimized\{RootDirectory.Name}\{childDir.FullName.Split(RootDirectory.Name)[1]}"))
                {
                    cmd = $@"/C mkdir {parent.FullName}\{RootDirectory.Name}_optimized\{RootDirectory.Name}\{childDir.FullName.Split(RootDirectory.Name)[1]}";
                    try
                    {
                        Process.Start("CMD.exe", cmd);
                    }
                    catch
                    {
                        Console.WriteLine("could not create directory for optimized images");
                    }
                }
            optimizedDir =  childDir == null ? new DirectoryInfo(parent.FullName + $@"\{RootDirectory.Name}_optimized\{RootDirectory.Name}") : new DirectoryInfo(parent.FullName + $@"\{RootDirectory.Name}_optimized\{RootDirectory.Name}\{childDir.FullName.Split(RootDirectory.Name)[1]}");
            return optimizedDir;
        }
    }
}
